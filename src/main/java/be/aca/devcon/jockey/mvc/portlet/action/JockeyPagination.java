package be.aca.devcon.jockey.mvc.portlet.action;

import java.io.IOException;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONSerializer;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;

import be.aca.devcon.jockey.mvc.portlet.json.Manipulator;
import be.aca.devcon.jockey.service.api.Jockey;
import be.aca.devcon.jockey.service.api.JockeyService;

@Component(
	immediate = true,
	property = {
			"javax.portlet.name=jockeyMvcPortlet",
			"mvc.command.name=jockeyPagination"
	},
	service = MVCResourceCommand.class
)
public class JockeyPagination implements MVCResourceCommand, Action {

	private static final Log LOGGER = LogFactoryUtil.getLog(JockeyPagination.class);

	private static final int NUMBER_PER_PAGE = 3;
	private static final int PAGE_OFFSET = 1;

	@Reference private JockeyService jockeyService;
	@Reference private Manipulator manipulator;

	public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws PortletException {
		Integer requestedPage = Integer.valueOf(resourceRequest.getParameter("page"));
		String selectedTeam = resourceRequest.getParameter("selectedTeam");

		LOGGER.info("Page requested: " + requestedPage);
		LOGGER.info("SelectedTeam: " + selectedTeam);

		int jockeysTotal = jockeyService.getJockeysCount(selectedTeam);
		int amountOfPages = jockeysTotal / NUMBER_PER_PAGE;
		int from = (requestedPage - PAGE_OFFSET) * NUMBER_PER_PAGE;
		int to = from + NUMBER_PER_PAGE;

		List<Jockey> jockeys = jockeyService.getJockeys(selectedTeam, from, to);
		JSONSerializer serializer = JSONFactoryUtil.createJSONSerializer();
		String json = serializer.serialize(jockeys);

		try {
			resourceResponse.getPortletOutputStream().write(json.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return true;
	}
}