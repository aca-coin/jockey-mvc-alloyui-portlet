package be.aca.devcon.jockey.mvc.portlet.action;

public interface Action {
	String JSP_PAGE_PARAM = "jspPage";
	String MAIN_VIEW = "/view.jsp";
	String DETAIL_VIEW = "/detail.jsp";
}