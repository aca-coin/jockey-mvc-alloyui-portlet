package be.aca.devcon.jockey.mvc.portlet.json;

import java.util.List;

import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONSerializer;

import be.aca.devcon.jockey.service.api.Jockey;
import be.aca.devcon.jockey.service.api.JockeyService;

@Component(
		immediate = true,
		service = Manipulator.class
)
public class Manipulator {

	@Reference private JockeyService jockeyService;

	public void addJockeys(PortletRequest request, List<Jockey> jockeys) {
		JSONSerializer serializer = JSONFactoryUtil.createJSONSerializer();
		String json = serializer.serialize(jockeys);

		request.setAttribute("jockeys", json);
	}

	public void addTeams(PortletRequest request) {
		List<String> teams = jockeyService.getTeams();
		request.setAttribute("teams", teams);
	}

	public void addTotal(PortletRequest request, int total) {
		request.setAttribute("totalJockeys", total);
	}
}