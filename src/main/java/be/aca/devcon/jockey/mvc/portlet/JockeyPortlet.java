package be.aca.devcon.jockey.mvc.portlet;

import java.io.IOException;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import be.aca.devcon.jockey.mvc.portlet.json.Manipulator;
import be.aca.devcon.jockey.service.api.JockeyService;

@Component(
		immediate = true,
		property = {
				"com.liferay.portlet.display-category=ACA Devcon",
				"com.liferay.portlet.instanceable=true",
				"com.liferay.portlet.css-class-wrapper=jockey-mvc-portlet",
				"javax.portlet.name=jockeyMvcPortlet",
				"javax.portlet.display-name=Jockey MVC Portlet",
				"javax.portlet.security-role-ref=power-user,user",
				"javax.portlet.init-param.template-path=/",
				"javax.portlet.init-param.view-template=/view.jsp"
		},
		service = Portlet.class
)
public class JockeyPortlet extends MVCPortlet {

	@Reference private Manipulator manipulator;
	@Reference private JockeyService jockeyService;

	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		int totalJockeys = jockeyService.getJockeysCount(null);

		manipulator.addTotal(renderRequest, totalJockeys);
		manipulator.addTeams(renderRequest);

		super.doView(renderRequest, renderResponse);
	}
}