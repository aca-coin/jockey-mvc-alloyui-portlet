package be.aca.devcon.jockey.mvc.portlet.action;

import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;

import be.aca.devcon.jockey.mvc.portlet.json.Manipulator;
import be.aca.devcon.jockey.service.api.Jockey;
import be.aca.devcon.jockey.service.api.JockeyService;

@Component(
		immediate = true,
		property = {
				"javax.portlet.name=jockeyMvcPortlet",
				"mvc.command.name=selectTeam"
		},
		service = MVCActionCommand.class
)
public class TeamSelection extends BaseMVCActionCommand implements Action {

	@Reference private JockeyService jockeyService;
	@Reference private Manipulator manipulator;

	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		String selectedTeam = actionRequest.getParameter("selectedTeam");
		List<Jockey> jockeys = jockeyService.getJockeys(selectedTeam);
		int totalJockeys = jockeyService.getJockeysCount(selectedTeam);

		manipulator.addJockeys(actionRequest, jockeys);
		manipulator.addTotal(actionRequest, totalJockeys);
		manipulator.addTeams(actionRequest);
		actionResponse.setRenderParameter(JSP_PAGE_PARAM, MAIN_VIEW);
	}
}