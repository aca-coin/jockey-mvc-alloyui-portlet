package be.aca.devcon.jockey.mvc.portlet.action;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;

import be.aca.devcon.jockey.service.api.Jockey;
import be.aca.devcon.jockey.service.api.JockeyService;

@Component(
		immediate = true,
		property = {
				"javax.portlet.name=jockeyMvcPortlet",
				"mvc.command.name=jockeyDetail"
		},
		service = MVCActionCommand.class
)
public class JockeyDetail extends BaseMVCActionCommand implements Action {

	@Reference private JockeyService jockeyService;

	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Integer selectedJockeyId = Integer.valueOf(actionRequest.getParameter("jockeyId"));
		Jockey jockey = jockeyService.getJockey(selectedJockeyId);

		actionRequest.setAttribute("jockey", jockey);
		actionResponse.setRenderParameter(JSP_PAGE_PARAM, DETAIL_VIEW);
	}
}