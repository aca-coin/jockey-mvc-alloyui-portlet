<%@ include file="/init.jsp" %>

<portlet:renderURL var="backToOverview" />

<div class="row">
	<div class="col-md-4">
		<h3>${jockey.name}</h3>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
		<img src="${jockey.portraitUrl}" alt="${jockey.name}"  />
	</div>
	<div class="col-md-6">
		<div class="row">
			<label>ID</label>
		</div>
		<div class="row">
			<label>#${jockey.id}</label>
		</div>
		<div class="row">
			<label>Horse</label>
		</div>
		<div class="row">
			<label>#${jockey.horse}</label>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<a href="${backToOverview}">Back to Overview</a>
	</div>
</div>