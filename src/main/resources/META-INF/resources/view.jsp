<%@ include file="/init.jsp" %>

<portlet:actionURL name="selectTeam" var="selectTeam" />

<aui:form action="<%= selectTeam %>">
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<aui:select name="selectedTeam" label="Select a team" showEmptyOption="true">
					<c:forEach items="${teams}" var="team">
						<aui:option value="${team}" selected="${team == selectedTeam}">${team}</aui:option>
					</c:forEach>
				</aui:select>
			</div>
		</div>
		<div class="col-md-2">
			<aui:button type="submit" />
		</div>
	</div>
</aui:form>

<div id="<portlet:namespace/>jockeytable"></div>
<div id="<portlet:namespace/>jockeyPagination" class="pagination pagination-lg"></div>

<aui:script use="aui-datatable, aui-pagination, liferay-portlet-url, aui-io-request">
	var columns = [
		{
			key: "id",
			label: 'ID'
		},
		{
			key: "name",
			label: 'Jockey',
			sortable: true
		},
		{
			key: "horse",
			label: 'Horse',
			sortable: true
		},
		{
			key: "teamName",
			label: 'Team',
			sortable: true
		},
		{
			key: '',
			label: '',
			formatter: function (cell) {
				var actionUrl = Liferay.PortletURL.createActionURL();
				actionUrl.setPortletMode("view");
				actionUrl.setWindowState("normal");
				actionUrl.setPortletId("${themeDisplay.getPortletDisplay().getId()}");
				actionUrl.setParameter("jockeyId", cell.data.id);
				actionUrl.setName("jockeyDetail");
				return '<a onClick="location.href = ' + "'" + actionUrl.toString() + "'" + '">Details</a>';
			},
			allowHTML: true
		}
	];

	var table = new A.DataTable({
		columns: columns,
		<%--data: ${jockeys},   as the ajax request for the pagination kicks in, we won't need to load data here--%>
	}).render("#<portlet:namespace/>jockeytable");

	var paginationUrl = Liferay.PortletURL.createResourceURL();
	paginationUrl.setPortletMode("view");
	paginationUrl.setWindowState("normal");
	paginationUrl.setPortletId("${themeDisplay.getPortletDisplay().getId()}");
	paginationUrl.setResourceId("jockeyPagination");

	new A.Pagination({
		contentBox: '#<portlet:namespace/>jockeyPagination',
		total: (${totalJockeys} / 3),
		page: 1,
		on: {
			changeRequest: function(event) {
				console.log(${selectedTeam});

				paginationUrl.setParameter("page", event.state.page);
				paginationUrl.setParameter("teamName", "${selectedTeam}");

				A.io.request(
					paginationUrl.toString(),
					{
						cache: "false",
						method: "GET",
						dataType: "json",
						<%--autoLoad: "false",  Does not seem to work? --%>
						on: {
							success: function() {
								var jockeys = this.get('responseData');
								table.set('data', jockeys);
							}
						}
					}
				);
			}
		}
	}).render();
</aui:script>